using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using weatherforecast.RestAPI;

namespace weatherforecast
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "weatherforecast", Version = "v1" });
            });

            services.AddSingleton<IHttpCallRequest, CHttpCallRequest>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            #region ForwardedHeaders
            var HeadersRequestProto = "X-Forwarded-Proto";
            var HeadersRequestHost = "X-Forwarded-Host";
            var HeadersRequestPort = "X-Forwarded-Port";
            var HeadersRequestPathBase = "X-Forwarded-Prefix";
            #endregion
            #region PathBase
            app.Use(async (context, next) =>
            {
                if (context.Request.Headers.TryGetValue(HeadersRequestPathBase, out var pathPrefix))
                {
                    context.Request.PathBase = pathPrefix.Last();

                    if (context.Request.Path.StartsWithSegments(context.Request.PathBase, out var path))
                    {
                        context.Request.Path = path;
                    }
                }

                await next();
            });
            #endregion
            #region Swagger
            if (Configuration["UseSwagger"] == "Y")
            {
                app.UseSwagger(c =>
                {
                    c.PreSerializeFilters.Add((swaggerDoc, httpRequest) =>
                    {
                        if (!httpRequest.Headers.ContainsKey(HeadersRequestHost)) return;
                        #region CheckProtocol
                        string RequestProtoSetting = Configuration["SwaggerProto"];
                        var checkSwaggerProto = string.IsNullOrWhiteSpace(RequestProtoSetting);
                        string HeaderProto = httpRequest.Headers[HeadersRequestProto];
                        string RequestProto = checkSwaggerProto ? HeaderProto : RequestProtoSetting;
                        #endregion
                        #region CheckPort
                        var swaggerPort = RequestProto == "http" ? Configuration["SwaggerHttp"] : Configuration["SwaggerHttps"];
                        var checkSwaggerPort = string.IsNullOrWhiteSpace(swaggerPort);
                        string xForwardedPort = httpRequest.Headers[HeadersRequestPort];
                        string RequestPort = checkSwaggerPort ? xForwardedPort : swaggerPort;
                        #endregion
                        var RequestHost = httpRequest.Headers[HeadersRequestHost];
                        var RequestPathBase = httpRequest.Headers[HeadersRequestPathBase];

                        var sbSwaggerServerUrl = new StringBuilder();
                        sbSwaggerServerUrl.Append(RequestProto);
                        sbSwaggerServerUrl.Append("://");
                        sbSwaggerServerUrl.Append(RequestHost);
                        if (RequestPort != "80" && RequestPort != "443")
                        {
                            sbSwaggerServerUrl.Append(":");
                            sbSwaggerServerUrl.Append(RequestPort);
                        }
                        sbSwaggerServerUrl.Append(RequestPathBase);

                        var serverUrl = sbSwaggerServerUrl.ToString();

                        swaggerDoc.Servers = new List<OpenApiServer>()
                        {
                            new OpenApiServer { Url = serverUrl }
                        };
                    });
                });
                app.UseSwaggerUI(c =>
                {
                    var swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                    c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "weatherforecast v1");
                });
            }
            #endregion
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
