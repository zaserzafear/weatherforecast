﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace weatherforecast.RestAPI
{
    public static class HttpRequestContentType
    {
        public const string not_use_content_type = null;
        public const string x_www_form_urlencoded = "application/x-www-form-urlencoded";
        public const string application_json = "application/json";
    }

    public static class HttpRequestLoadBalancer
    {
        public const string not_use_lb = null;
        public const string api_internal_alb = "api-internal-alb";
    }

    public interface IHttpCallRequest
    {
        Task<HttpResponseFromRequest> CallAPI(string url, HttpMethod method, string content_type = HttpRequestContentType.not_use_content_type, string loadBalancer = HttpRequestLoadBalancer.not_use_lb, Dictionary<string, string> header = null, Dictionary<string, dynamic> param = null, HttpHeaderAuthorization headerAuthorization = null);
    }

    public class CHttpCallRequest : IHttpCallRequest
    {
        private readonly IConfiguration config;

        public CHttpCallRequest(IConfiguration config)
        {
            this.config = config;
        }

        public async Task<HttpResponseFromRequest> CallAPI(string url, HttpMethod method, string content_type = HttpRequestContentType.not_use_content_type, string loadBalancer = HttpRequestLoadBalancer.not_use_lb, Dictionary<string, string> header = null, Dictionary<string, dynamic> param = null, HttpHeaderAuthorization headerAuthorization = null)
        {
            var requestUri = new Uri(url);
            // Get Request Information Before Request To Load Balancer
            var scheme = requestUri.Scheme;
            var host = requestUri.Host;
            var port = requestUri.Port;
            var pathAndQuery = requestUri.PathAndQuery;
            // Create HttpClient
            HttpClient client = new HttpClient();
            // Clear All Default Header
            client.DefaultRequestHeaders.Clear();
            // Check Load Balancer Before Call API
            if (loadBalancer != HttpRequestLoadBalancer.not_use_lb)
            {
                var sb_call_lb_uri = new StringBuilder();
                sb_call_lb_uri.Append(scheme);
                sb_call_lb_uri.Append("://");
                sb_call_lb_uri.Append(config[loadBalancer]);
                if (port != 80 && port != 443)
                {
                    sb_call_lb_uri.Append(":");
                    sb_call_lb_uri.Append(port);
                }
                sb_call_lb_uri.Append(pathAndQuery);

                var call_loadbalancer_uri = sb_call_lb_uri.ToString();
                client.BaseAddress = new Uri(call_loadbalancer_uri);
                client.DefaultRequestHeaders.Add("Host", host);
                // Set Header Authorization
                if (headerAuthorization != null)
                {
                    if (headerAuthorization.Authorization != null)
                    {
                        client.DefaultRequestHeaders.Add("Authorization", headerAuthorization.Authorization);
                    }
                    if (headerAuthorization.AuthorizationJWT != null)
                    {
                        client.DefaultRequestHeaders.Add("AuthorizationJWT", headerAuthorization.AuthorizationJWT);
                    }
                }
            }
            // Not Use Load Balancer
            else
            {
                client.BaseAddress = requestUri;
            }
            // Create HttpRequestMessage
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = method;
            // Add Headers
            if (header != null)
            {
                foreach (var item in header)
                {
                    request.Headers.Add(item.Key, item.Value);
                }
            }
            // Add Param
            if (method != HttpMethod.Get && param != null)
            {
                switch (content_type)
                {
                    case HttpRequestContentType.application_json:
                        request.Content = JsonContent.Create(param);
                        break;
                    case HttpRequestContentType.x_www_form_urlencoded:
                        request.Content = new FormUrlEncodedContent((IEnumerable<KeyValuePair<string, string>>)param);
                        break;
                    default:
                        break;
                }
            }
            // Get HttpResponseMessage
            HttpResponseMessage response = await client.SendAsync(request);
            int response_status = ((int)response.StatusCode);
            string response_body = response.Content.ReadAsStringAsync().Result;
            string response_header = response.Headers.ToString();
            // Return Result
            return new HttpResponseFromRequest { status = response_status, body = response_body, headers = response_header };
        }
    }

    public class HttpHeaderAuthorization
    {
        public string Authorization { get; set; }
        public string AuthorizationJWT { get; set; }
    }

    public class HttpResponseFromRequest
    {
        public int status { get; set; }
        public string body { get; set; }
        public string headers { get; set; }
    }
}
