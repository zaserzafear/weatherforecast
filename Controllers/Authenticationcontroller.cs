﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using weatherforecast.RestAPI;

namespace weatherforecast.Controllers
{
    public class Authenticationcontroller : Controller
    {
        private readonly IConfiguration _config;
        private readonly IHttpCallRequest _callRequest;

        public Authenticationcontroller(IConfiguration config, IHttpCallRequest callRequest)
        {
            _config = config;
            _callRequest = callRequest;
        }

        [HttpPost("api/[controller]/auth")]
        public async Task<IActionResult> Auth([FromBody] ParamAuthen incomeParam)
        {
            var url = _config["jwt-web-api"];
            var path = _config["jwt-web-api-auth-path"];
            var fullUrl = $"{url}{path}";
            var param = new Dictionary<string, dynamic>();
            param.Add("username", incomeParam.Username);
            param.Add("password", incomeParam.Password);
            param.Add("authen_type", incomeParam.AuthenType);
            var headerAuthorization = new HttpHeaderAuthorization
            {
                Authorization = _config["Authorization"]
            };
            var response = await _callRequest.CallAPI(url: fullUrl, method: System.Net.Http.HttpMethod.Post, content_type: HttpRequestContentType.application_json, loadBalancer: HttpRequestLoadBalancer.api_internal_alb, param: param, headerAuthorization: headerAuthorization);
            return StatusCode(response.status, response);
        }

        [HttpGet("api/[controller]/hello")]
        public async Task<IActionResult> Hello()
        {
            return await Task.FromResult(Ok("Hello"));
        }
    }

    public class ParamAuthen
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int AuthenType { get; set; }
    }
}
